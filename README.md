mvn gatling:test -Dgatling.simulationClass=simulations.S00_PligorFirstTest

You only need to change the simulations.S00_PligorFirstTest

OR for multiple simulations one after the other you have but you need to configure pom.xml first:
mvn gatling:test -Pperfcollection

===================
Remember there is a Gatling plugin for Jenkins

Download the .war file of jenkins from their website (google it!)
And run jenkins from terminal: java -jar jenkins.war --httpPort=9090

