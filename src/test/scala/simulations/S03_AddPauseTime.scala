package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt

class S03_AddPauseTime extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Pligor First scenario").
    exec(
      http("Grab all games")
        .get("videogames")
    ).pause(1, 20) //between 1 and 20 seconds
    .exec(
      http("Call specific videogame")
        .get("videogames/1")
    ).pause(5) //fixed 5 seconds

    .exec(
      http("Again all video games").get("videogames")
    ).pause(3000.milliseconds)

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}