package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class S07_CodeReuse extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private def grab_all_games() = {
    exec(
      http("Grab all games")
        .get("videogames")
        .check(substring("Resident"))
        .check(jsonPath("$[1].id").saveAs("game_id"))
    )
  }

  private def grab_specific_game(game_id_session_var_name: String) = exec(
    http("Call specific videogame")
      .get("videogames/${" + game_id_session_var_name + "}")
      .check(jsonPath("$.name").is("Gran Turismo 3")) //https://jsonpath.com/
      .check(status.in(200 to 210))
      .check(bodyString.saveAs("body_string"))
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Repeating")
    .repeat(3) {
      grab_all_games().pause(1, 2) //between 1 and 20 seconds
        .exec { session =>
          assert(session.attributes.contains("game_id"))
          session
        }.repeat(5)({
        grab_specific_game("game_id")
      })
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}