package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.gatling.http.request.builder.HttpRequestBuilder

class S06_ExtractData extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Checking the Response Code").
    exec(
      http("Grab all games")
        .get("videogames")
        .check(substring("Resident"))
        .check(jsonPath("$[1].id").saveAs("game_id"))
    ).pause(1, 2) //between 1 and 20 seconds
    .exec { session =>
      println("AAAAAAAAAAAAAAAAAAAAAAAAAA")
      println(session.attributes.get("game_id"))
      session
    }
    .exec(
      http("Call specific videogame")
        .get("videogames/${game_id}")
        .check(jsonPath("$.name").is("Gran Turismo 3")) //https://jsonpath.com/
        .check(bodyString.saveAs("body_string"))
    )
    .exec { session =>
      println("BODYYYYYYYYYYYYYYYYYY")
      println(session("body_string").as[String]) //the as[String] is necessary otherwise it is an entire object with many attributes
      session
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}