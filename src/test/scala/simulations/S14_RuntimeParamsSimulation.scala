package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt

/**
 * This is for getting parameters from the command line
 *
 * mvn gatling:test -Dgatling.simulationClass=simulations.S14_RuntimeParamsSimulation -DFINAL_USERS=30 -DRAMP_DURATION=5
 */
class S14_RuntimeParamsSimulation extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  private def get_property(property_name: String, default_value: String) = {
    Option(System.getenv(property_name)) ////////////////// WE ALSO ALLOW ENVIRONMENT VARIABLES TO BE PASSED HERE
      .orElse(
        Option(System.getProperty(property_name)))  ////////////////// WE ALSO ALLOW SYSTEM PROPERTIES TO BE PASSED HERE
      .getOrElse(default_value)
  }

  private def userCount: Int = get_property("FINAL_USERS", "5").toInt
  private def duration: Int = get_property("RAMP_DURATION", "10").toInt

  before {
    println(s"Running test with ${userCount} users")
    println(s"Running test with ramp duration of ${duration} seconds")
  }

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private def grab_all_games() = {
    exec(
      http("Grab all games")
        .get("videogames")
        .check(status.is(200))
      //        .check(jsonPath("$[1].id").saveAs("game_id"))
    )
  }

  private def grab_specific_game() = exec(
    http("Call specific videogame")
      .get("videogames/2")
      .check(jsonPath("$.name").is("Gran Turismo 3")) //https://jsonpath.com/
      .check(status.is(200))
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Parameters from command line Load Simulation")
    .exec { session =>
      //assert(session.contains("game_id"))
      //println("session game id: " + session("game_id").as[String])
      session
    }.exec(grab_all_games())
    .pause(5, 10)
    .exec(grab_specific_game())
    .pause(5, 10)
    .exec(grab_all_games())

  // 3 Load scenario
  setUp(
    myscenario.inject(
      nothingFor(5.seconds),
      //constantUsersPerSec(10).during(10.seconds),
      //rampUsersPerSec(1).to(5).during(20.seconds),
      heavisideUsers(userCount).during(duration.seconds),
    )
  ).protocols(http_conf.inferHtmlResources())
    //.maxDuration(1.minute)

}