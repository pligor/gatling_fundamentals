package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import io.gatling.http.request.builder.HttpRequestBuilder

class S05_CheckResponseBody extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private def commonReqCheck(hrb: HttpRequestBuilder) = hrb.check(status.is(200))

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Checking the Response Code").
    exec(
      commonReqCheck(
        http("Grab all games")
          .get("videogames")
          .check(substring("Resident"))
      )
    ).pause(1, 2) //between 1 and 20 seconds
    .exec(
      commonReqCheck(
        http("Call specific videogame")
        .get("videogames/1")
          .check(jsonPath("$.name").is("Resident Evil 4")) //https://jsonpath.com/
      )
    ).pause(1, 2) //fixed 5 seconds

    .exec(
      http("Again all video games").get("videogames")
        .check(status.not(440))
        .check(jsonPath("$[0].name").is("Resident Evil 4")) //https://jsonpath.com/
    )

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}