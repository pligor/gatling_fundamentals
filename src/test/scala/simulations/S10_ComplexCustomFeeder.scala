package simulations

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.util.Random
import scala.util.parsing.json.JSONObject

class S10_ComplexCustomFeeder extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private val id_numbers = (11 to 20).iterator

  private val data_pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  def randomString(length: Int): String = {
    Random.alphanumeric.filter(_.isLetter).take(length).mkString
  }

  def randomDate(startDate: LocalDate): String = {
    startDate.minusDays(Random.nextInt(30)).format(data_pattern)
  }

  private val customFeeder = Iterator.continually(Map(
    "game_id" -> id_numbers.next(),
    "name" -> ("Game-" + randomString(5)),
    "releaseDate" -> randomDate(LocalDate.now()),
    "reviewScore" -> Random.nextInt(100),
    "category" -> ("Category-" + randomString(6)),
    "rating" -> ("Rating-" + randomString(4))
  )) //so later gameId is going to be a Gatling session variable

  // call session variable with dollar and curly brackets
  private def post_new_game() = exec(
    http("Call specific videogame")
      .post("videogames/")
      .body(ElFileBody("body/NewGameTemplate.json")).asJson
      .check(status.is(200))
    //.check(jsonPath("$.name").is("${game_name}")) //https://jsonpath.com/
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Posting new games with a Complex Custom feeder")
    .repeat(10) {
      feed(customFeeder).exec { session =>
        assert(session.contains("game_id"))
        println("session game id: " + session("game_id").as[String])
        //println("releaseDate: " + session("releaseDate").as[String])
        session
      } .exec(
        post_new_game()
      )
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}