package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class S01_PligorFirstTest extends Simulation {
  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json")

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Pligor First scenario").
    exec(
      http("Grab all games")
        .get("/videogames")
    )

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}