package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class S08_CsvFeeder extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private val csv_feeder = csv("data/game_data.csv")
    .circular //csv feeder strategy

  private def grab_all_games() = {
    exec(
      http("Grab all games")
        .get("videogames")
        .check(substring("Resident"))
        .check(jsonPath("$[1].id").saveAs("game_id"))
    )
  }

  private def grab_specific_game() = exec(
    http("Call specific videogame")
      .get("videogames/${game_id}")
      .check(status.in(200 to 210))
      .check(jsonPath("$.name").is("${game_name}")) //https://jsonpath.com/
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Working with csv feeder")
    .repeat(10) {
      feed(csv_feeder).exec { session =>
        assert(session.contains("game_id"))
        assert(session.contains("game_name"))
        println(session("game_id").as[String] + ": " + session("game_name").as[String])
        session
      }.exec(
        grab_specific_game()
      )
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}