package simulations

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt
import scala.util.Random

class S11_BasicLoadSimulation extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private def grab_all_games() = {
    exec(
      http("Grab all games")
        .get("videogames")
        .check(status.is(200))
      //        .check(jsonPath("$[1].id").saveAs("game_id"))
    )
  }

  private def grab_specific_game() = exec(
    http("Call specific videogame")
      .get("videogames/2")
      .check(jsonPath("$.name").is("Gran Turismo 3")) //https://jsonpath.com/
      .check(status.is(200))
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Basic Load Simulation")
    .repeat(2) {
      //feed(customFeeder).
      exec { session =>
        //assert(session.contains("game_id"))
        //println("session game id: " + session("game_id").as[String])
        session
      }.exec(grab_all_games())
        .pause(2,  3)
        .exec(grab_specific_game())
        .pause(2, 4)
        .exec(grab_all_games())
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(
      nothingFor(10.seconds),
      atOnceUsers(10),
      rampUsers(200).during(30.seconds)
    )
  ).protocols(http_conf.inferHtmlResources())

}