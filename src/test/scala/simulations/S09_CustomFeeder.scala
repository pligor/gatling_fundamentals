package simulations

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

class S09_CustomFeeder extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://localhost:8080/app/").
    header("Accept", "application/json");

  private val id_numbers = (1 to 10).iterator

  private val customFeeder = Iterator.continually(
    Map("game_id" -> id_numbers.next())) //so later gameId is going to be a Gatling session variable

  // call session variable with dollar and curly brackets
  private def grab_specific_game() = exec(
    http("Call specific videogame")
      .get("videogames/${game_id}")
      .check(status.in(200 to 210))
      //.check(jsonPath("$.name").is("${game_name}")) //https://jsonpath.com/
  )

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Working with a Custom feeder")
    .repeat(10) {
      feed(customFeeder).exec { session =>
        assert(session.contains("game_id"))
        println("session game id: " + session("game_id").as[String])
        //assert(session.contains("game_name"))
        //println(session("game_id").as[String] + ": " + session("game_name").as[String])
        session
      }.exec(
        grab_specific_game()
      )
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(atOnceUsers(1))
  ).protocols(http_conf)

}