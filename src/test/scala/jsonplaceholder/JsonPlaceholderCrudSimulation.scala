package jsonplaceholder

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.DurationInt
import scala.util.Random

/**
 * https://jsonplaceholder.typicode.com/
 *
 * This is for getting parameters from the command line
 mvn gatling:test -Dgatling.simulationClass=jsonplaceholder.JsonPlaceholderCrudSimulation \
   -DMAX_USERS_PER_SEC=3 -DRAMP_DURATION_SEC=10 -DTOTAL_DURATION_MINS=10
 */
class JsonPlaceholderCrudSimulation extends Simulation {
  //Never forget: //https://gatling.io/docs/current/cheat-sheet/

  private def get_property(property_name: String, default_value: String) = {
    Option(System.getenv(property_name)) ////////////////// WE ALSO ALLOW ENVIRONMENT VARIABLES TO BE PASSED HERE
      .orElse(
        Option(System.getProperty(property_name))) ////////////////// WE ALSO ALLOW SYSTEM PROPERTIES TO BE PASSED HERE
      .getOrElse(default_value)
  }

//  private def user_count = get_property("FINAL_USERS", "5").toInt
  private def max_user_per_sec: Int = get_property("MAX_USERS_PER_SEC", "3").toInt
  private def ramp_duration: Int = get_property("RAMP_DURATION_SEC", "10").toInt
  private def total_duration: Int = get_property("TOTAL_DURATION_MINS", "2").toInt

  before {
//    println(s"Running test with ${user_count} users")
    println(s"Running test with ${max_user_per_sec} users")
    println(s"Running test with ramp duration of ${ramp_duration} seconds")
  }

  private def get_all_comments() = {
    exec(
      http("Get all comments")
        .get("comments/")
        .check(status.is(200))
        .check(jsonPath("$[499]").exists) //verify that it has length of 500
    )
  }

  private def create_a_comment() = {
    exec(
      http("Create a new comment")
        .post("comments/")
        .body(ElFileBody("body/comment.json")).asJson
        .check(status.is(201))
        .check(jsonPath("$.id").is("501").saveAs("created_id")) //in this dummy api the newly created comment is always 501
    )
  }

  private def get_created_comment() = exec(
    http("Get specific comment")
      .get("comments/${created_id}")
      .check(status.is(404)) //the newly created comment unfortunately is not really saved
  )

  private def get_existing_comment() = exec(
    http("Get specific comment")
      .get("comments/1")
      .check(status.is(200)) //the newly created comment unfortunately is not really saved
      .check(jsonPath("$.id").is("1"))

  )

  private def delete_created_comment() = exec(
    http("Delete created comment")
      .delete("comments/${created_id}")
      .check(status.is(200)) //the newly created comment unfortunately is not really saved
      .check(bodyString.transform(_.length).is(2)) //we are expecting a body of size exactly 2
  )

  def randomString(text_len: Option[Int] = None): String = {
    val randomTextLen = text_len.getOrElse(Random.nextInt(100))
    Random.alphanumeric.filter(_.isLetter).take(randomTextLen).mkString
  }

  private val customFeeder = Iterator.continually(Map(
    "postId" -> 1, //let's keep this fixed, we don't care so much
    "name" -> ("Name-" + randomString()),
    "email" -> (randomString() + "@" + randomString() + "." + randomString(Some(3))),
    "body" -> ("Comment body - " + randomString()),
  )) //so later postId etc. are going to be a Gatling session variables


  // 1 Http conf
  val http_conf: HttpProtocolBuilder = http.baseUrl("http://jsonplaceholder.typicode.com/")
    .header("Accept", "application/json")
  //      .proxy(Proxy("localhost", 8888))

  // 2 Scenario Definition
  val myscenario: ScenarioBuilder = scenario("Load Simulation on poor Json Placeholder").
    forever {
      feed(customFeeder)
        .exec(get_all_comments())
        .pause(1, 3)
        .exec { session =>
          assert(session.contains("postId"))
          assert(session.contains("name"))
          assert(session.contains("email"))
          assert(session.contains("body"))
          println(
            "Attempting to create a new comment with post id: " + session("postId").as[String] +
              "\nand name: " + session("name").as[String] +
              "\nand email: " + session("email").as[String] +
              "\nand body: " + session("body").as[String]
          )
          session
        }.exec(create_a_comment())
        .pause(1, 3)
        .exec { session =>
          assert(session.contains("created_id"))
          println("created id: " + session("created_id").as[String])
          session
        }
        .exec(get_created_comment())
        .pause(1, 3)
        .exec(get_existing_comment())
        .pause(1, 3)
        .exec(delete_created_comment())
        .pause(1)
    }

  // 3 Load scenario
  setUp(
    myscenario.inject(
//      atOnceUsers(1),
      //      nothingFor(5.seconds),
      //      heavisideUsers(userCount).during(duration.seconds),
      //constantUsersPerSec(10).during(10.seconds),
      rampUsersPerSec(1).to(max_user_per_sec).during(ramp_duration.seconds),
    )
  ).protocols(http_conf.inferHtmlResources())
    .maxDuration(total_duration.minutes)

  after {
    println("This is the END of the Test")
  }
}